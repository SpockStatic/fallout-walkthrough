package com.example.fallout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity{

   /* Button b1;
    Button b2;
    Button b3;
    Button b4;
    Button b5;
    Button b6;
    Button b7;
    Button b8;
    Button b9;
    Button b10;
    Button b11;
    Button b12;
    Button b13;
    Button b14;
    Button b15;
    Button b16;*/



    public static final int[] idArray = {R.id.vault13,R.id.TheHub01,R.id.Vault15,R.id.Junktown,R.id.Necropolis,R.id.Vault1302,R.id.TheHub02,R.id.TheGlow,R.id.TB01,R.id.TheHub03,R.id.Adytown,R.id.MBase01,R.id.TB02,R.id.MBase02,R.id.TheCathedral,R.id.TheEnd,R.id.goHome };

    Button[] button = new Button[idArray.length];
    int i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);


        for (i=0; i<idArray.length;i++){

        button[i] = (Button)findViewById(idArray[i]);
        button[i].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (view.getId()){
                    case R.id.vault13:   setContentView(R.layout.activity_vault2);
                    break;
                    case R.id.TheHub01:   setContentView(R.layout.activity_the_hub01);
                    break;
                    case R.id.Vault15:   setContentView(R.layout.activity_vault15);
                        break;
                    case R.id.Junktown:   setContentView(R.layout.activity_junktown);
                        break;
                    case R.id.Necropolis:   setContentView(R.layout.activity_necropolis);
                        break;
                    case R.id.Vault1302:   setContentView(R.layout.activity_vault1302);
                        break;
                    case R.id.TheHub02:   setContentView(R.layout.activity_the_hub02);
                        break;
                    case R.id.TheGlow:   setContentView(R.layout.activity_the_glow);
                        break;
                    case R.id.TB01:     setContentView(R.layout.activity_the_brotherhood01);
                        break;
                    case R.id.TheHub03:   setContentView(R.layout.activity_the_hub03);
                        break;
                    case R.id.Adytown:   setContentView(R.layout.activity_adytown);
                        break;
                    case R.id.MBase01:   setContentView(R.layout.activity_mbase01);
                        break;
                    case R.id.TB02:   setContentView(R.layout.activity_the_brotherhood02);
                        break;
                    case R.id.MBase02:   setContentView(R.layout.activity_mbase02);
                        break;
                    case R.id.TheCathedral:   setContentView(R.layout.activity_the_cathedral);
                        break;
                    case R.id.TheEnd:   setContentView(R.layout.activity_the_end);
                        break;
                    case R.id.goHome:   openHome();
                        break;


                }


            }
        });


        }


     /*   b1 = findViewById(R.id.vault13);
        b2 = findViewById(R.id.TheHub01);
        b3 = findViewById(R.id.Vault15);
        b4 = findViewById(R.id.Junktown);
        b5 = findViewById(R.id.Necropolis);
        b6 = findViewById(R.id.Vault1302);
        b7 = findViewById(R.id.TheHub02);
        b8 = findViewById(R.id.TheGlow);
        b9 = findViewById(R.id.TB01);
        b10 = findViewById(R.id.TheHub03);
        b11 = findViewById(R.id.Adytown);
        b12 = findViewById(R.id.MBase01);
        b13 = findViewById(R.id.TB02);
        b14 = findViewById(R.id.MBase02);
        b15 = findViewById(R.id.TheCathedral);
        b16 = findViewById(R.id.TheEnd);
        b17 = findViewById(R.id.goHome);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_vault2);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_vault3);
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_junktown);
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_hub01);
            }
        });

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_necropolis);
            }
        });

        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_vault1302);
            }
        });

        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_hub02);
            }
        });

        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_glow);
            }
        });

        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_brotherhood01);
            }
        });

        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_hub03);
            }
        });

        b11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_adytown);
            }
        });

        b12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_mbase01);
            }
        });

        b13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_brotherhood02);
            }
        });

        b14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_mbase02);
            }
        });

        b15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_cathedral);
            }
        });

        b16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.activity_the_end);
            }
        });

        b17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openHome();
            }
        });*/


    }

   @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, Main3Activity.class);
        startActivity(intent);
    }

    public void openHome(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    }

