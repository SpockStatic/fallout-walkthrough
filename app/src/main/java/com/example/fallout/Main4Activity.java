package com.example.fallout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class Main4Activity extends AppCompatActivity {

    Button generate;
    TextView Str;
    TextView End;
    TextView Per;
    TextView Int;
    TextView Cha;
    TextView Agi;
    TextView Luc;

    Random r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        r = new Random();

       generate = findViewById(R.id.generator);
       Str = findViewById(R.id.Str);
       End = findViewById(R.id.End);
       Per = findViewById(R.id.Per);
       Int = findViewById(R.id.Int);
       Cha = findViewById(R.id.Char);
       Agi = findViewById(R.id.Agi);
       Luc = findViewById(R.id.Luc);

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Random rand = new Random();
                int number = rand.nextInt(10)+1;
                int number1 = rand.nextInt(10)+1;
                int number2= rand.nextInt(10)+1;
                int number3 = rand.nextInt(10)+1;
                int number4 = rand.nextInt(10)+1;
                int number5 = rand.nextInt(10)+1;
                int number6 = rand.nextInt(10)+1;

                TextView Str = (TextView)findViewById(R.id.Str);
                TextView End = (TextView)findViewById(R.id.End);
                TextView Per = (TextView)findViewById(R.id.Per);
                TextView Int = (TextView)findViewById(R.id.Int);
                TextView Cha = (TextView)findViewById(R.id.Char);
                TextView Agi = (TextView)findViewById(R.id.Agi);
                TextView Luc = (TextView)findViewById(R.id.Luc);

                String myString = String.valueOf(number);
                String myString1 = String.valueOf(number1);
                String myString2 = String.valueOf(number2);
                String myString3 = String.valueOf(number3);
                String myString4 = String.valueOf(number4);
                String myString5 = String.valueOf(number5);
                String myString6 = String.valueOf(number6);



                Str.setText(myString);
                End.setText(myString1);
                Per.setText(myString2);
                Int.setText(myString3);
                Cha.setText(myString4);
                Agi.setText(myString5);
                Luc.setText(myString6);

            }
        });


    }


}
